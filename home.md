---
title: README
description: The README page of the Pins team's Wiki.js instance.
published: true
date: 2020-05-01T10:48:44.581Z
tags: hello, world!, community hub
---

# Wiki README
Welcome to the Pins team's Wiki.js instance! While this wiki is currently work-in-progress,

## FAQs
[See this page instead](/faq) for the full list of frequently asked questions about our Wiki.js instance in Heroku.

## Contribute to the wiki
For now, we're not yet allowed anyone to contribute to the wiki yet. Stay tuned!